plugins {
    kotlin("multiplatform")
    kotlin("native.cocoapods")
    id("com.android.library")
    id("org.jetbrains.compose")
    id(Plugin.Id.kover)
    id("app.cash.sqldelight") version "2.0.0-alpha04"
}

version = "1.0-SNAPSHOT"

kotlin {
    android()

    jvm("desktop")
    js(IR) {
        browser() {
            testTask {
                useKarma {
                    useFirefoxHeadless()
                }
            }
        }
    }

    ios()
    iosSimulatorArm64()

    cocoapods {
        summary = "Shared code for the sample"
        homepage = "https://github.com/JetBrains/compose-jb"
        ios.deploymentTarget = "14.1"
        podfile = project.file("../iosApp/Podfile")
        framework {
            baseName = "shared"
            isStatic = true
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(compose.runtime)
                implementation(compose.foundation)
                implementation(compose.material)
//                implementation(Deps.Compose.circuit)
                api(Deps.Log.kermit)
                api(Deps.Kotlinx.coroutines)
                api(Deps.Koin.core)
                api(Deps.Kor.korio)

                implementation(Deps.Matrix.Client)
                with(Deps.Utility) {
                    api(mpsettings)
                    api(mpsettingsNoArgs)
                    api(compose.ui)
                }
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
                with(Deps.Test) {
                    implementation(koin)
                    implementation(coroutines)
                }
            }
        }
        val androidMain by getting {
            dependencies {
                implementation("androidx.appcompat:appcompat:1.5.1")
                implementation("androidx.core:core-ktx:1.9.0")
                implementation(Deps.Sqldelight.sqliteAndroidDriver)
            }
        }
        val iosMain by getting {
            dependencies {
                implementation(Deps.Sqldelight.sqliteNativeDriver)
            }
        }
        val iosTest by getting
        val iosSimulatorArm64Main by getting {
            dependsOn(iosMain)
        }
        val iosSimulatorArm64Test by getting {
            dependsOn(iosTest)
        }

        val desktopMain by getting {
            dependencies {
                implementation(compose.desktop.common)
                implementation(Deps.Sqldelight.sqliteJvmDriver)
            }
        }
        val jsMain by getting {
            dependencies {
                npm("@matrix-org/olm", "3.2.13")
                implementation(Deps.Sqldelight.sqliteJsDriver)
            }
        }
        val jsTest by getting {}
    }
}

sqldelight {
    database("ChiaChatDb") { // This will be the name of the generated database class.
        packageName = "org.chiachat.app"
        dialect(Deps.Sqldelight.sqliteDialect)
        deriveSchemaFromMigrations = true
        verifyMigrations = true
    }
}


android {
    compileSdk = 33
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    sourceSets["main"].res.srcDirs("src/androidMain/res")
    sourceSets["main"].assets.srcDirs("src/commonMain/resources")
    defaultConfig {
        minSdk = 24
        targetSdk = 33
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    lint {
        baseline = file("lint-baseline.xml")
    }
}
