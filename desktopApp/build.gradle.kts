import org.jetbrains.compose.desktop.application.dsl.TargetFormat

plugins {
    kotlin("multiplatform")
    id("org.jetbrains.compose")
}

kotlin {
    jvm {
        compilations.all { kotlinOptions.jvmTarget = "17" }
        withJava()
    }
    sourceSets {
        val jvmMain by getting {
            dependencies {
                implementation(compose.desktop.currentOs)
                implementation(project(":shared"))
            }
        }
    }
}

val assets = File("../shared/src/commonMain/resources/assets" )
val icons = assets.resolve("icons/chiachat")
val ico = icons.resolve("chiachat-trans-256x256.ico")
val icns = icons.resolve("chiachat-trans-256x256.icns")
val png = icons.resolve("chiachat-trans-2048x2048.png")

compose.desktop {
    application {
        mainClass = "org.chiachat.app.desktop.MainKt"

        buildTypes.release.proguard {
            this.isEnabled.set(true)
            configurationFiles.from(project.file("compose-desktop.pro"))
        }

        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "ChiaChat"
            packageVersion = "1.0.0"
            modules("java.sql")

            windows {
                menu = true
                // see https://wixtoolset.org/documentation/manual/v3/howtos/general/generate_guids.html
                upgradeUuid = "f850ec60-faad-435c-944c-8bf566d5ffa8"
                iconFile.set(ico)
            }

            macOS {
                bundleID = "org.chiachat.app"
                iconFile.set(icns)
            }

            linux {
                iconFile.set(png)
                packageName = "chiachat"
                debMaintainer = "andrea@chiachat.org"
            }
        }
    }
}
