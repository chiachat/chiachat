package org.chiachat.app.desktop

import DesktopRoot
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.WindowState
import androidx.compose.ui.window.application
import androidx.compose.ui.window.singleWindowApplication
import androidx.compose.ui.window.Window

val desktopRoot = DesktopRoot()

fun main() = application {
    val icon = painterResource("assets/icons/chiachat/chiachat-trans-2048x2048.png")
    Window(onCloseRequest = ::exitApplication, icon = icon) { desktopRoot.View() }
}